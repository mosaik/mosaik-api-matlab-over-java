%% Simulator
% This is an abstract simulator class that serves as an API to mosaik.
% It should be called by the Java class MatlabSimulator over matlabcontrol.
% Create a non-abstract simulator class inheriting from this class. As a
% base class for simulator models, use the class 'model'.
% Please feel free to use (by commenting/uncommenting) the irregularly indented
% debugging outputs.

classdef (Abstract) Simulator < handle  %inheriting from handle class to make attribute changes persistent.
%% Standard properties
    properties (SetAccess = protected)
        simName
        SimID
        metastruct = struct('models',struct(),'api_version','2.0');
        initiated = false;
        parallelize = false;
        numCores = 0;
        step_size
        entities = struct();
    end

%%    
    methods        
%% Simulator Constructor
% Takes <simName> as input string
        function obj = Simulator(simName)
            disp(['Executing MATLAB Constructor for: ', simName])
     
            obj.simName = simName;

        end
        
        
%% metadata = init(obj, json_sim_params)
% Initializes the Simulator using <json_sim_params> and returns the
% <metadata>
        function metadata = init(obj, json_sim_params, sid)
            disp('Matlabs Simulator.init method called.')        
            %global DEBUG
            %if DEBUG
            %    save('./DEBUG/init_workspace')
            %end        
    
            %% Standard
            obj.initiated = true;         
            sim_params = loadjson(json_sim_params);

            obj.SimID = sid;
            
            if isfield(sim_params,'step_size')
                obj.step_size = sim_params.step_size;
            end            
            if isfield(sim_params,'parallelize')
                obj.parallelize = sim_params.parallelize;
            end            
            if isfield(sim_params,'numCores')
                obj.numCores = sim_params.numCores;
            end
            
            if obj.parallelize
                if obj.numCores == 0
                    obj.numCores = feature('numcores')
                end
                pool = gcp('nocreate'); % If no pool, do not create new one.
                if isempty(pool)
                    parpool('local',obj.numCores);
                elseif pool.NumWorkers ~= obj.numCores %not equal
                    delete(pool);
                    parpool('local',obj.numCores);
                end
            end
            metadata = savejson('', obj.metastruct); %convert metastruct to json metadata
        end
        
        %% created = create(obj,number, model_type, json_model_params)
        % Creates <number> instances of the simulator model <model_type>
        % using the <model_params> for all these instances. Returns the
        % created instances as a sturct() called Entities.
        % The Entity IDs <Eid> are numbered continously for every model, even if
        % some entities of the requested model already exist.
        % <json_model_params> should be a Json object Containing the initialization
        % parameters for the models.
        function created = create(obj,number, model_type, json_model_params)
            disp('Matlabs Simulator.create method called.')
            global DEBUG
            if DEBUG
               save('./DEBUG/create_workspace')
            end

            model_params = loadjson(json_model_params);
 
            %Perhaps just necessary for calls from Matlab:
            % Check if model_params contain a cell array.
            % Converting that cell array to struct array, because this is
            % probably a conversion error from loadjson.
            fnames = fieldnames(model_params);
            for i = 1: length(fnames)
                if iscell(model_params.(fnames{i}))
                    %keyboard
                    disp('Cell entry found in model params during model creation. Converting to struct.')
                    model_params.(fnames{i}) = myCell2Struct(model_params.(fnames{i}));
                end
            end
            
            
            function [ OutputStruct ] = myCell2Struct( InputCells )
                %Cell to Struct cenversion: [ OutputStruct ] = myCell2Struct( InputCells )
                %   Converts a 1xN Cell Array containing Struct with equal fields
                %   to a 1xN Struct array containing the respective fields.
                %   Realized as a nested function, because it is only needed at
                %   this point.

                if ~ min(size(InputCells))==1
                    error('At leasrt one dimension of the input cell array must be 1.')
                end

                for j = 1:length(InputCells)
                    if ~isstruct(InputCells{j})
                        error('CellArray contains non-struct-element')
                    end

                    if exist('OutputStruct','var')
                        OutputStruct = [OutputStruct,InputCells{j}];
                    else
                        OutputStruct = InputCells{j};
                    end
                end
            end            
                    
            %% Standard: Creating entities           
            Entities = struct();
            created_list = cell(number,1);            
            
            % Check if the simulator already has models.
            no_old_fields = 0;
            if ~isempty(fieldnames(obj.entities))
                % If so, how many of the requested kind?
                old_fieldnames = fieldnames(obj.entities);
                for k = 1:numel(old_fieldnames)
                	no_old_fields = no_old_fields + strcmp(obj.entities.(old_fieldnames{k}).type,model_type);
                end
            end
            
            % Add their number to all new entitiy counters.
            if obj.parallelize
                entitiesSliced = cell(number,1);
                parfor i = 1+no_old_fields:number+no_old_fields
                    % Generate entity ID
                    Eid = sprintf('%s_%s',model_type,num2str(i));
                    % Create entity (default) 
                    Entity = commitEval(model_type,Eid,model_params,obj.SimID);
                    % Append createt entity to struct
                    entitiesSliced{i} = Entity;

                    %Create nested list of created objects
                    created_list{i - no_old_fields} = struct;
                    created_list{i - no_old_fields}.eid = Entity.Eid;
                    created_list{i - no_old_fields}.type = Entity.type;
                    created_list{i - no_old_fields}.rel = {};
                    created_list{i - no_old_fields}.children = {};
                end
                for i = 1+no_old_fields:number+no_old_fields
                    obj.entities.(entitiesSliced{i}.Eid) = entitiesSliced{i};
                end
            else
                for entity_counter = 1+no_old_fields:number+no_old_fields
                    % Generate entity ID
                    Eid = sprintf('%s_%s',model_type,num2str(entity_counter));
                    % Create entity (default) 
                	evalString = sprintf('%s(''%s'',model_params,obj.SimID) ',model_type,Eid);
                    Entity = eval(evalString);
                    % Append createt entity to struct
                    obj.entities.(Entity.Eid) = Entity;

                    %Create nested list of created objects
                    created_list{entity_counter - no_old_fields} = struct;
                    created_list{entity_counter - no_old_fields}.eid = Entity.Eid;
                    created_list{entity_counter - no_old_fields}.type = Entity.type;
                    created_list{entity_counter - no_old_fields}.rel = {};
                    created_list{entity_counter - no_old_fields}.children = {};
                end
            end
            
            created_struct = myCell2Struct(created_list);
            
%%
            created = savejson('', created_struct);
            
            if numel(created_list) == 1 %If only entity was created, savejson does not add the []-brackets indicating the list type for java.
               created = ['[   ',created,'    ]'];
            end  

            %created = created(2:end-2) %savejson liefert ein set von klammern zu viel, was in python fuer fehler sorgt.
        end
        
        %% recall_time = step(obj,current_time, json_inputs, iteration)
        % <json_inputs> should be a JSONObject mapping all the simulator models 
        % (by their <Eid>s) to their specific input parameters. Optionally,
        % <inputs> may also contain a parameter <step_size>. In that case, this
        % specific step will be executed with a different step_size than that which
        % is specified as a simulator property.
        % <iteration> is only needed if some models make a getData request that
        % they want to have the answer to before the next step. <iteration> starts
        % out at 1. If any model of the simulator set his <model.call_me_again> to
        % true, those models will be called a second time in the same step from
        % java with <iteration> = 2.
        function return_json = step(obj,current_time, json_inputs, iteration)
            inputsEmpty = false;
            inputs = struct();
            if  strcmp(json_inputs,'{}')
                warning('Simulator step() method recieved no inputs (json_inputs == {})!')
                inputsEmpty = true;
            else
                % Convert JSON to struct                
                inputs = loadjson(json_inputs,'UseMap',true);
            end
            %Set Stepsize to default if not specified in inputs
            if inputsEmpty || ~inputs.isKey('step_size')
                this_step_size = obj.step_size;
            else
                this_step_size = inputs('step_size');
            end
            
            [recall_time, SimRequests] = standard_step(obj, iteration, inputs, inputsEmpty);
            
            call_sim_again = 0;
            
            %% Standard step procedure: call all model step functions
            function [recall_time, SimRequests] = standard_step(obj, iteration, inputs, inputsEmpty) % nested function
                
                ModelNames = sort(fieldnames(obj.entities)); %sorted, to make shure that
                num_models = numel(ModelNames);
                SimRequests = struct();
                call_sim_again = 0;
                ModelRequests(1:num_models) = struct('asyncs', containers.Map());

                % Walk through all models and call their step functions. 
                if obj.parallelize
                    call_sim_again_sliced = zeros(1,num_models);
                    models = cell(num_models,1);
                    for i=1:num_models
                        models{i} = obj.entities.(ModelNames{i});
                    end
                    parfor i = 1:num_models               
                        if iteration==1 || models{i}.call_me_again 
                            if inputsEmpty
                                send_inputs = struct();
                            else
                                send_inputs = inputs(ModelNames{i});
                            end
                            %Step models - Returns a conatiner.Map
                            [ModelRequests(i).asyncs,models{i}] = models{i}.step(send_inputs, this_step_size, current_time); 
                        end
                        % call again as long as one or more models need further calls.
                        call_sim_again_sliced(i) = models{i}.call_me_again;
                    end
                    for i=1:num_models
                        SimRequests.(ModelNames{i}) = ModelRequests(i).asyncs;
                        obj.entities.(ModelNames{i}) = models{i};
                        
                        call_sim_again = max(call_sim_again,call_sim_again_sliced(i));
                        %disp(['SimRequests: ',ModelRequests(i).asyncs]);
                    end
                else
                    for i=1:num_models
                        this_model = obj.entities.(ModelNames{i});
                        if iteration==1 || this_model.call_me_again 
                            if inputsEmpty
                                send_inputs = struct();
                            else
                                send_inputs = inputs(ModelNames{i});
                            end
                            %Step models - Returns a conatiner.Map
                            [ModelRequests(i).asyncs] = this_model.step(send_inputs, this_step_size, current_time); 
                            SimRequests.(ModelNames{i}) = ModelRequests(i).asyncs;
                        end
                        % call again as long as one or more models need further calls.
                        call_sim_again = max(call_sim_again,this_model.call_me_again);
                    end    
                end
                recall_time = int64(current_time + this_step_size);
            end
           
            %SimRequests_json = savejson('', SimRequests);
            return_struct = struct('recall_time',recall_time,...
                'SimRequests', SimRequests, 'call_sim_again', call_sim_again);

            return_json = savejson('',return_struct,'ForceRootName',true,'FormatVersion',1.9);
	    %save('./DEBUG/step_return_json','return_json')
            %save('./DEBUG/step_return_struct','return_struct')
        end
        
        
%% Json_Answer = getData(obj,requested)
% This method is invoked if another simulator requests data from this
% simulator.
% <requested> should be a JSONObject mapping Model-IDs of <Requested_Entities>
% to the names of their <Requested_Fields>.
% For calls from MATLAB, <requested> may also be passed directly as a struct.
% Returns <Json_Answer> as a simple json Object just like <requested>, but
% mapping values to the rewuested fields.
        function Json_Answer = getData(obj,requested)
%     disp('Matlab getData called')
%     global DEBUG
%     if DEBUG
%        save('./DEBUG/getData_workspace')
%     end         

            Answers = struct(); % Predefine answer struct
            
            % Convert JSON to structs
            if ischar(requested)
                RequestStruct = loadjson(requested);
            elseif isstruct(requested)
                RequestStruct = requested;
            else
                error('Unknown format of argument "requested"');
            end
            Requested_Entities = fieldnames(RequestStruct);
            
%% Standard: Walking through requestet entities
            for n=1:numel(Requested_Entities) 
                % Check if the entity is valid:
                if isfield(obj.entities,Requested_Entities{n}) 
                    
                    % Read requested fields of this entity
                    Requested_Fields = getfield(RequestStruct,Requested_Entities{n}); %#ok<*GFLD>

                    Fields = struct();
                    % walk through requested fields (mostly just one...)
                    for m = 1:numel(Requested_Fields)
                        if isprop(getfield(obj.entities,Requested_Entities{n}),Requested_Fields{m})
                            % Read requestet entity values
                            Fields = setfield(Fields, Requested_Fields{m},...
                                getfield(getfield(obj.entities,Requested_Entities{n}),...%model object
                                    Requested_Fields{m})); %#ok<*SFLD>
                        else
                            warning('Requested Field %s in Entity %s could not be found.',Requested_Fields{m},Requested_Entities{n});
                        end                        
                    end 
                    
                    % Append read fields to the answer struct
                    Answers = setfield(Answers, Requested_Entities{n}, Fields);
                else
                    warning('Requested Entity %s could not be found.',Requested_Entities{n});
                end
            end  
%%

            % Convert to JSON
            Json_Answer = savejson('', Answers,'ArrayToStruct',0);
    %disp('Matlab getData executed')
        end
        
        
%% destructor: delete(obj)
        function delete(obj)          
              disp(['Destructing Simulator ', obj.simName]);
        end   
                
    end
    
end
