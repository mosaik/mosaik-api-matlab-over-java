classdef ElectricMeter < model
    %Stromzaehler 
    %   Electricity Meter 
    %   Simulator model for an electricity meter that integrates the used
    %   electric power of a connected entity over time.
    properties
        Energy %Integrated Energy
        Power %Current Power
        conn_entity
    end
    
    methods
        function em = ElectricMeter(Eid, params, SimID)
            em@model(Eid, params, SimID);
            em.Energy = params.init_val;
            em.type = 'ElectricMeter';
            em.Power = 0;
            %disp(params)
        end
        
        function [asyncs,em] = step(em, inputs, step_size, time, ~)             
            asyncs=containers.Map();

            try
                [~, power] = em.unravel_inputs(inputs, 'Power'); %check inputs for errors
            catch
                warning(['No Power-Input recieved by ' em.Eid])
                return
            end
                                                          
            %actual calculation
            em.Power = power;            
            em.Energy = em.Energy+(em.Power*step_size);
            
            %Define return value
            recall_time = time + step_size;
        end
     
    end
    
end

