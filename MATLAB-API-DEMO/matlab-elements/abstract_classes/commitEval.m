function [entity] = commitEval(model_type,eid,model_params,simID)
    evalString = sprintf('%s(''%s'',model_params,simID) ',model_type,eid);
    entity = eval(evalString);
end