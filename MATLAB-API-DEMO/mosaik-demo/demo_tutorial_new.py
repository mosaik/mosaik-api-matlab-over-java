__author__ = 'rengel'
import itertools
import random
import time
import json
from subprocess import call
from mosaik.util import connect_randomly, connect_many_to_one
import mosaik
import os


SIM_CONFIG = {
    'MExampleSim': {
        'cmd': 'java -jar MatlabSimulator.jar %(addr)s MExampleSim',
        'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    },
    'Collector': {
        'python': 'collector:Collector',
    },
}

END = 10 * 60  # 10 minutes

def main():
    random.seed(23)
    world = mosaik.World(SIM_CONFIG)
    create_scenario(world)
    world.run(until=END)  # As fast as possilbe
    # world.run(until=END, rt_factor=1/60)  # Real-time 1min -> 1sec

# Create World
# world = mosaik.World(SIM_CONFIG)

def create_scenario(world):

    # Start simulators
    examplesim = world.start('MExampleSim', eid_prefix='Model_')
    collector = world.start('Collector', step_size=60)

    # Instantiate models
    model = examplesim.MModel.create(1, init_val=2)[0]
    #model = examplesim.MExampleModel.create(1, init_val=2)
    print(model)
    monitor = collector.Monitor()

    # Connect entities
    #print(model)
    # print(monitor)
    world.connect(model, monitor, 'val', 'delta')

    # Create more entities
    more_models = examplesim.MModel.create(2, init_val=3)
    #print(more_models)
    mosaik.util.connect_many_to_one(world, more_models, monitor, 'val', 'delta')

# Run simulation
# world.run(until=END)


if __name__ == '__main__':

    no_mat_sessions = 1 #number of MATLAB sessions needed (= number of matlab simulators)
    debug_matlab = True #open matlab hidden (for quick running) or openly (for debugging)
    cwd = SIM_CONFIG.get('MExampleSim').get('cwd')  #matlab folder path
    if(debug_matlab):
        debug_param = "debug"
    else:
        debug_param = "nodebug"

    #calls the Script that calls the java class that preopens MATLAB, then waits for initialization to complete
    os.system('java -cp StartMatlab.jar de.offis.mosaik.api.matlab.StartMatlab ' + cwd + ' ' + str(no_mat_sessions) + ' ' + debug_param)
    print('Waiting for MATLAB to get ready')
    time.sleep(10)

    main()


