classdef MatBatSim < Simulator
    %Battery Simulator class
    
    properties
        %entities = struct();
    end
    
    methods
%% CONSTRUCTOR
        function obj = MatBatSim(simName)
            obj@Simulator(simName);
            
            battery_meta = struct(); %Define Metadata for counter-model
            battery_meta.public = true;
            battery_meta.params = {'age','batterycells'}; %creation-params
            battery_meta.attrs = {'conn_entity','type','Eid',...
                'age', 'E_max', 'P_set', 'P_get', 'E', 'requested_power','SoC'}; % acessable attrs
                            % 'E' gibt es nicht direkt als eigenschaft, muss aber fur get_Data zuganglich sein
            battery_meta.any_inputs = true;
            
            obj.metastruct.models = struct('Battery',battery_meta);
        end    
    end
    
end

