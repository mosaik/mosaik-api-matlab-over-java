classdef Battery < model
    % Very basic battery model, to demonstrate mosaik interfacace.
    % To demonstrate how user defined objects can be integrated into the
    % model, the battery model consists of several batterycells, which are
    % entities of the user defined class BatteryCell. The user defined
    % classes Battery and BatteryCell provide methods to translate their
    % actual state into a struct, which is platform independent and which
    % can be send to mosaik. Mosaik starts a new matlab process and with
    % the state information in the struct it can instantiate a new battery
    % object in the new process, which is an exact copy of the original
    % battery. 
    
    properties (SetAccess = private)
        age; % battery age [sec]
        E_max; % max energy content of battery [J]
        E;      % Current stored energy
        P_get;  % real average power of battery [W]
                % positive values: charge battery,
                % netative values: discharge battery     
        P_set;  % desired average power of battery in next time step [W], 
                % positive values: charge battery,
                % netative values: discharge battery
        SoC;
               
    end
    
    properties (Access = public)
        conn_entity;
    end
    
    properties (SetAccess = immutable)       
        batterycells;  % battery consists of several cells [1xn BatteryCell]
    end
    
    methods
        function battery = Battery(Eid, battery_params, SimID)  % constructor
            battery@model(Eid, battery_params, SimID);
            battery.type = 'Battery';

            % set initial battery age, if not specified 
            % set default zero
            if isfield(battery_params, 'age')
                battery.age = battery_params.age;
            else
                battery.age = 0;  % default value
            end
            
            battery.Eid = Eid;

            % instantiate cells
            % battery_params.batterycell = [1xn struct]
            battery.batterycells = BatteryCell.empty;  % [0x0 BatteryCell]
            for cell_params = battery_params.batterycells
                battery.batterycells(end+1) = BatteryCell(cell_params);
            end

            % calculate E_max of battery as sum of cells E_max
            battery.E_max = 0;
            for batterycell = battery.batterycells
                battery.E_max = battery.E_max + batterycell.E_max;
            end

            
            %Get current total stored energy
            [battery.SoC,battery.E] = get_charge_level(battery);
        end
        
        function asyncs = step(battery, inputs ,step_size, iteration)
            %% scenario-specific step dunction
            % In this scenario, the battery communicates with a
            % ChargeControlSim and a FuelCell. It recieves an input
            % power-offer  from both of them and charges/discharges
            % for the sum of those powers as much as it can. 
            % It then returns via setData how much of the Power it could
            % use.
            % 
            % Scenario-funtionality:
            % For demonstration, this demonstrates two different 
            % control-machanisms.
            % The FuelCell notices how much power was used and fits its
            % behavior to this information.
            % The ChargeControl model is "stupid" and anly knows on/of.
            % We set its "may_produce" parameter via setData
            %
            % From the FuelCell should only be charged if the SoC of the
            % battery is more than 20% smaller than that of the fuelCell.
            % Otherwise, the battery shall charge the fuelcell.
            %
            % getData-demonstration:
            % Because the Battery requests the SoC of the fuelCell 
            % via getData and wants the answer in the same step,
            % we need to use the switch iteration in the step-function.
            % The async call getData will be created in the first
            % iteration, executed from java between iterations, and the
            % answer can be used in iteration two.
            asyncs = containers.Map();
            
            switch iteration
                case 1                   
                    %% handling the async-requests
                    
                    % just to know the connected entities
                    [entitynames_absender, ~] = battery.unravel_inputs(inputs, 'P_set');


                    
                    % Decide if more charging from the CC is necessary
                    if get_charge_level(battery)<0.95 
                        charge_more = true;
                    else
                        charge_more = false;
                    end
                    % find the address of the CCs from the inputs
                    for address = entitynames_absender 

                        if (~isempty(strfind(address{1},'ChargeControlSim')) || ~isempty(strfind(address{1},'RPGSim')))
                            %creating a may_produce call to the ChargeControl
                            asyncs = createAsyncRequest(battery, asyncs, 'set', address{1}, 'may_produce', charge_more);
                        end
                    end
                    
                    % find the address of the FuelCell from the inputs
                    for address = entitynames_absender 
                        if logical(strfind(address{1},'FCSimulator'))
                            %creating a getData request for SoC from the
                            %FuelCellSim
                            asyncs = createAsyncRequest(battery, asyncs, 'get', address{1}, 'SoC');
                            
                            battery.call_me_again = true; %step() is not done yet. Call again!
                        end
                    end

                case 2

                    % read the inputs
                    [asyncs_absender, AsyncAnswers] = battery.unravel_inputs(inputs, 'AsyncAnswers'); %Read asnc Answers
                    [power_offerings_absender, offered_power] = battery.unravel_inputs(inputs, 'P_set'); %Read P_set -> offered power
                    FC_SoC = AsyncAnswers('SoC');
                    
                    %This is just to remember who offered how much power
                    %was supplied by which address in order to accept the
                    %power in the same proportions
                    partof_offered_power(1)= offered_power{1}/sum([offered_power{:}]); 
                    partof_offered_power(2)= offered_power{2}/sum([offered_power{:}]);
                    partof_offered_power(isnan(partof_offered_power))=0;
                    offered_power = sum([offered_power{:}]);

                    %decide if the FC should produce or consume on its next
                    %call. 
                    if battery.SoC < FC_SoC
                        %FC produces only if its 20% higher charged than the battery
                        FC_mode = 'produce'; 
                    else
                        FC_mode = 'consume';
                    end
                    asyncs = createAsyncRequest(battery, asyncs, 'set', asyncs_absender{1}, 'produce_or_consume', FC_mode);

                    
                    %% Regular battery step.    
                    set_power_el(battery,-offered_power);
                    n_cells = length(battery.batterycells); % number of cells
                    P_set_cell = battery.P_set / n_cells;
                    % distribute power equally to cells
                    [battery.SoC,battery.E] = get_charge_level(battery);
                    battery.P_get = 0; %P_get becomes the accepted power
                    for batterycell = battery.batterycells % step all cells
                        battery.P_get = battery.P_get + ...
                        batterycell.step(P_set_cell, step_size); 
                    end
                    [battery.SoC,battery.E] = get_charge_level(battery);
                    %% setData commands for FuelCell and ChargeControl

                    asyncs = createAsyncRequest(battery, asyncs, 'set', power_offerings_absender{1}, 'accepted_power', battery.P_get*partof_offered_power(1));
                    asyncs = createAsyncRequest(battery, asyncs, 'set', power_offerings_absender{2}, 'accepted_power', battery.P_get*partof_offered_power(2));

                    battery.call_me_again = false; %Done with step()
            end

            
            
            %update total current stored energy 
            [battery.SoC,battery.E] = get_charge_level(battery);
%fprintf('Battery charge level: %d \n',get_charge_level(battery));

        end
        
        function set_power_el(battery, P_el)
            % sets the desired average power for the next time step [Watt]
            % postive value: discharge battery
            % negative value: charge battery
            battery.P_set = -P_el;
            % Attention: Internally opposite sign convention
        end
            
        function P_el = get_power_el(battery)
            % returns the actual power of the last time step [Watt]
            % postive value: battery discharging
            % negative value: battery charging
            P_el = -battery.P_get;
            % Attention: Internally opposite sign convention
        end
        
        function [L,E] = get_charge_level(battery)
            % returns the actual charging state
            % 0: empty, 1: fully charged
            E = 0;
            for batterycell = battery.batterycells
                E = E + batterycell.E;
            end
            L = E / battery.E_max;
        end
        
        function P_max = get_P_el_max(battery)
            % returns maximum power of battery
            P_max = 0;
            for batterycell = battery.batterycells
                P_max = P_max - batterycell.P_min; 
                % P_min and minus because of reversed internal sign convention
            end
        end
           
        function P_min = get_P_el_min(battery)
            % returns minimum power of battery
            P_min = 0;
            for batterycell = battery.batterycells
                P_min = P_min - batterycell.P_max; 
                % P_max and minus because of reversed internal sign convention
            end
        end
            
        function battery_state = get_state(battery)
            % translates battery object to struct
            % -> platform independent representation of Battery object
            % which is needed by mosaik to instantiate copies of Battery
            % objects in a new matlab process
            % Battery(battery.get_state()) delivers copy of
            % battery
            len = length(battery.batterycells);
            cell_states(len) = battery.batterycells(end).get_state();
            % initialize struct array of appropriate size to take take 
            % structs representing the cells
            i = 1;
            for batterycell = battery.batterycells(1:end-1);
                cell_states(i) = batterycell.get_state();
                i = i+1;
            end
            battery_state = struct( ...
                'age', battery.age, ...
                'batterycells', cell_states ...
                );
        end
        
        function battery_copy = copy(battery)
            % delivers exact copy of battery, which is completly indipendent
            % from original battery and which is initilized with the actual
            % state of the original battery
            battery_copy = Battery(battery.get_state());
        end
    end
end