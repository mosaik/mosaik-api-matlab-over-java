classdef ChargeControlSim < Simulator
    %FCSimulator: This is a Simulator for a dummy-fuelcell with very limited
    %functionality
    
    properties
    end
    
    methods
            %Concrete class Constructor
            function CCSim = ChargeControlSim(simName)
            CCSim@Simulator(simName);
            %obj@Simulator(simName);

            
            CC_meta = struct(); %Define Metadata for model
            CC_meta.public = true;
            CC_meta.params = {'may_produce'};
            CC_meta.attrs = {'power_out','type','Eid','input_power','may_produce'};
            CC_meta.any_inputs = true;
            
            CCSim.metastruct.models = struct('ChargeControl',CC_meta);
                  
            end
    end
    
end

