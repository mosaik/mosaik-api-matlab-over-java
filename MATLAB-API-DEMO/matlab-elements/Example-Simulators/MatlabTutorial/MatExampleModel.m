classdef MatExampleModel < model
    %   Matlab Example Model
    properties
   	    val
        delta
        init_val
        eid_prefix
    end
    
    methods
        function ex = MatExampleModel(Eid, params, SimID)
            ex@model(Eid, params, SimID);
            ex.type = 'MatExampleModel';
	    ex.delta=1;
            ex.val = params.init_val;
	    ex.init_val=0;
            ex.eid_prefix='Model_';	
            
            %disp(ex)
        end
        
        
        function asyncs = step(ex, inputs, step_size, time,~)
            asyncs = containers.Map();
            
            try
               [~, tmpDelta]= ex.unravel_inputs(inputs,'delta'); %check inputs for errors
            catch
               warning('No Delta-Input received by' ex.Eid)
               return
            end

            if iscell(tmpDelta) && (numel(tmpDelta)==1)
                if isnan(tmpDelta{1})
                    warning('Was not able to get attribute delta from inputs.');
                end
            else
                ex.delta=tmpDelta;
            end 

            try
               [~, tmpVal]= ex.unravel_inputs(inputs,'val'); %check inputs for errors
            catch
               warning('No Input-Value received by' ex.Eid)
               return
            end

            if iscell(tmpVal) && (numel(tmpVal)==1)
                if isnan(tmpVal{1})
                    warning('Was not able to get attribute val from inputs.');
                end
            else
                ex.val=tmpVal; %Save if input is valid
            end 

            %% perform step            
            if sum(ex.delta)==1
                ex.val = ex.val + ex.delta;
            end
            
         end
    end
end