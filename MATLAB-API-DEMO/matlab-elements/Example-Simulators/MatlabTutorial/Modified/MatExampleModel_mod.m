classdef MatExampleModel_mod < model
    %   Matlab Example Model
    properties
   	    val
        delta
        init_val
        eid_prefix
        step_size
    end
    
    methods
        function ex = MatExampleModel_mod(Eid, params, SimID)
            ex@model(Eid, params, SimID);
            ex.type = 'MatExampleModel_mod';
            ex.delta=1;
            ex.val = params.init_val;
            %ex.init_val=0;
            ex.eid_prefix='Model_';	
            ex.step_size = 60;
            
            %disp(ex)
        end
        
        
        function asyncs = step(ex, inputs, step_size, time,~)
            asyncs = containers.Map();
            disp(ex)
%             try
%                [~, tmpDelta]= ex.unravel_inputs(inputs,'delta'); %check inputs for errors
%             catch
%                warning(['No Delta-Input received by' ex.Eid])
%                return
%             end
% 
%             if iscell(tmpDelta) && (numel(tmpDelta)==1)
%                 if isnan(tmpDelta{1})
%                     warning('Was not able to get attribute delta from inputs.');
%                 end
%             else
%                 ex.delta=tmpDelta;
%             end 

%             try
%                [~, tmpVal]= ex.unravel_inputs(inputs,'val'); %check inputs for errors
%             catch
%                warning(['No Input-Value received by ' ex.Eid])
%                return
%             end
% 
%             if iscell(tmpVal) && (numel(tmpVal)==1)
%                 if isnan(tmpVal{1})
%                     warning('Was not able to get attribute val from inputs.');
%                 end
%             else
%                 ex.val=tmpVal; %Save if input is valid
%             end 

            %% perform step            
%             if sum(ex.delta)==1
            ex.val = ex.val + ex.delta;
%             end
            disp(ex)
            %Define return value
            recall_time = time + step_size;
            
         end
    end
end