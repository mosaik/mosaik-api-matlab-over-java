classdef MSimulator < handle
    
    properties
       models = MModel.empty
       data = {}
       
    end
    
    methods
        
        function obj = add_model(obj,Eid, params, SimID)
            model = MModel(Eid, params, SimID);
            obj.models = [obj.models,model];
            obj.data = [obj.data,{0}];
        end
        
        function obj = step(obj)
            for i = 1:1: length(obj.models)
                model = obj.models(i);
                model = model.step();
                if obj.data{1,i}(1,1) == 0
                    obj.data{1,i}(1,1) = model.val;
                else
                    obj.data{1,i} =[obj.data{1,i},model.val];
                end

            end
            
        end
    end
end