classdef MatExampleSim_mod < Simulator
    %MatExampleSim: This is a Simulator for a Matlab Example Model
    %functionality   
    properties
    end  

    methods
            %Concrete class Constructor
            function ExSim = MatExampleSim_mod(simName)
            	ExSim@Simulator(simName);
            	%obj@Simulator(simName);
            
            	Ex_meta = struct(); %Define Metadata for model
            	Ex_meta.public = true;
            	Ex_meta.params = {'init_val','empty'};
            	Ex_meta.attrs = {'started','val','delta','type','Eid','eid_prefix','step_size'};
            	Ex_meta.any_inputs = true;
            
            	ExSim.metastruct.models = struct('MatExampleModel_mod',Ex_meta);
                  
            end    
    end
end