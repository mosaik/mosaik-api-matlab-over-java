Authors
=======

The mosaik matlab API was initially developed by Robin Engel.
It was extended and updated by Pranay Kasturi and Jan Sören Schwarz.
