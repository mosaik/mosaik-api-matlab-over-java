package de.offis.mosaik.api.matlab;

import matlabcontrol.*;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class StartMatlab {
    private static final Logger logger = LoggerFactory.getLogger("StartMatlab");
    
	public static void main(String[] args)
		throws MatlabConnectionException, MatlabInvocationException	{
		logger.info("Starting StartMatlab method.");	
		// create proxy
		MatlabProxyFactoryOptions options =  new MatlabProxyFactoryOptions.Builder()
					.setUsePreviouslyControlledSession(true).setHidden(true)
					.build();

		logger.debug("Debug string: " + args[2]);	
		
		String debugOrNot = new String(args[2]);
		if(debugOrNot.equals("debug")){
			MatlabProxyFactoryOptions debugging_options = new MatlabProxyFactoryOptions.Builder()
						.setUsePreviouslyControlledSession(true)
						.build();
			options = debugging_options;
		}
		
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		
		logger.debug("Created MatlabProxyFacotry");
		
		int sessions = 0;
		
		if(args.length ==0) {
			sessions = 1;
		} else {
			sessions = Integer.parseInt(args[1]);
		}
		
		logger.debug("Number of sessions: " + sessions);
		
		List<MatlabProxy> SessionList = new ArrayList<MatlabProxy>();

		for(int k=0;k < sessions;k++) {
			SessionList.add(factory.getProxy());
			// call builtin function
			SessionList.get(k).eval("disp('Matlab Initiated from batch file.')");
//			SessionList.get(k).eval("cd "+ System.getProperty("user.dir"));
			SessionList.get(k).eval("cd "+ args[0]);
			SessionList.get(k).eval("addpath(genpath('.'))");
		}
		
		logger.info("Started Matlab instances.");
		
		// close connections
		for(int l=0;l < sessions;l++) {  
			SessionList.get(l).disconnect();
		}
		
		logger.info("Closed connections to Matlab instances.");
	}		
}