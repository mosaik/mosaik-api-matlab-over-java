======
Errors
======

Connecting to MATLAB Sessions
-----------------------------
Python can call the mosaik-matlab API by a command line opening the .jar-file MatlabSimulator.jar. This
file can reconnect to an existing connected MATLAB-session. Theoretically, it can also open and
connect to a new session. However, this often results in the generation of an error (see respective
message below). There is a possibility that mosaik attempts to communicate with the matlab
session before the initiation is complete. The solution is the pre-initialization-scripts. If working on a very slow computer or initializing a lot of MATLAB sessions, it could happen that the initialization time for the MATLAB sessions exceeds mosaik's limit (
that limit might be three or five minutes). In that case, just starting the script again will reconnect to the
already opened sessions and open the amount that is required beyond that.

Restarting MATLAB after closing does not work
---------------------------------------------
Directly after closing a MATLAB session, opening a new one with the StartMatlab Java class
sometimes does not work. The reason might be that the old session is not completely closed.
If this happens, just abort the running batch file and start it anew.

Web connection warnings
-----------------------

Long simulations sometimes produce the following warning: ::

        WARNING: mosaik web . s e r v e r : socket ConnectionError in Server . handler ( ) 

A 100 Hours example simulation produced 3 of these warnings without a noticeable impact
on the simulations functionality. This warning possibly indicates a lost package of data
somewhere in the simulation.

JSON conversion errors
----------------------

Single parameters in the *metastruct* 'params' or 'attrs':  

The fields 'params' or 'attrs' of the *metastruct* variable are given as cell-arrays. If these cell-arrays
contain only a single field, jsonlab converts the cell array as though it weren't a cell array but
a simple string. This leads to complications in the conversion to java maps. Therefore, at least
two parameters must be specified. If only one argument is needed, an empty-variable can be
added that contains only a blank space.


Error-Message: MATLAB session not connected
-------------------------------------------
This is a common Error-message upon unsuccessful connection to a MATLAB-session: ::

        Exception in thread "main" java.lang.reflect.InvocationTargetException
                at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
                at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
                at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
                at java.lang.reflect.Method.invoke(Unknown Source)
                at org.eclipse.jdt.internal.jarinjarloader.JarRsrcLoader.main(JarRsrcLoader.java:58)
        Caused by: matlabcontrol.MatlabConnectionException: Support code location could not be determined. Could not get path from URI location.
        URI Location: jar:rsrc:matlabcontrol-4.1.0.jar!/
        URL Location: jar:rsrc:matlabcontrol-4.1.0.jar!/
        Code Source: (jar:rsrc:matlabcontrol-4.1.0.jar!/ <no signer certificates>)
        Protection Domain: ProtectionDomain  (jar:rsrc:matlabcontrol-4.1.0.jar!/ <no signer certificates>)
         java.net.URLClassLoader@677327b6
         <no principals>
         java.security.Permissions@70177ecd (
         ("java.security.AllPermission" "<all permissions>" "<all actions>")
        )


        Class Loader: java.net.URLClassLoader@677327b6
        Class Loader Class: class java.net.URLClassLoader
                at matlabcontrol.Configuration.getSupportCodeLocation(Configuration.java:295)
                at matlabcontrol.RemoteMatlabProxyFactory.createProcess(RemoteMatlabProxyFactory.java:289)
                at matlabcontrol.RemoteMatlabProxyFactory.requestProxy(RemoteMatlabProxyFactory.java:125)
                at matlabcontrol.RemoteMatlabProxyFactory.getProxy(RemoteMatlabProxyFactory.java:144)
                at matlabcontrol.MatlabProxyFactory.getProxy(MatlabProxyFactory.java:81)
                at MatlabSimulator.<init>(MatlabSimulator.java:54)
                at MatlabSimulator.main(MatlabSimulator.java:34)
                ... 5 more


